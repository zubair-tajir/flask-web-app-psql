import click
import json
from flask import current_app, g
from flask.cli import with_appcontext
from psycopg2 import sql, extras, connect as db_connect

class DBConnection:
    def __init__(self, dbname=None, user=None, password=None, host=None):
        filename = "metadb.json"
        with open(filename, 'r') as f:
            datastore = json.load(f)

        if dbname is None:
            dbname = datastore["dbname"]
        if user is None:
            user = datastore["user"]
        if password is None:
            password = datastore["password"]
        if host is None:
            host = datastore["host"]

        self.__conn = db_connect(dbname=dbname, user=user, password=password, host=host)
    
    def conn(self):
        return self.__conn

    def commit(self):
        """Commits work."""
        self.__conn.commit()

    def cursor(self):
        """Returns a cursor to the DB. Caller is responsible for closing the cursor."""
        return self.__conn.cursor()

def get_db():
    if 'db' not in g:
        obj = DBConnection()
        g.db = obj.conn()
    return g.db

def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()

def init_db():
    db = get_db()
    with current_app.open_resource('schema.sql') as f:
        db.cursor().execute(f.read().decode('utf8'))

@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    