import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from app.db import get_db
from psycopg2 import sql, extras, connect as db_connect

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        else:
            with db.cursor() as cursor:
                query = 'SELECT id FROM "user" WHERE username = %s;'
                cursor.execute(query, (username,))
                user = cursor.fetchone()
                if user is not None:
                    error = 'User {} is already registered.'.format(username)

        if error is None:
            with db.cursor() as cursor:
                query = 'INSERT INTO "user" (username, password) VALUES (%s, %s);'
                cursor.execute(query, (username, generate_password_hash(password)))
                db.commit()
                
                return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        with db.cursor(cursor_factory=extras.DictCursor) as cursor:
            query = 'SELECT * FROM "user" WHERE username = %s;'
            cursor.execute(query, (username,))
            user = cursor.fetchone()
            
            if user is None:
                error = 'Incorrect username.'
            elif not check_password_hash(user['password'], password):
                error = 'Incorrect password.'

            if error is None:
                session.clear()
                session['user_id'] = user['id']
                return redirect(url_for('index'))

            flash(error)

    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        db = get_db()
        with db.cursor(cursor_factory=extras.DictCursor) as cursor:
            query = 'SELECT * FROM "user" WHERE id = %s;'
            cursor.execute(query, (user_id,))
            g.user = cursor.fetchone()

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
